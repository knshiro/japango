class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :omniauthable

  validates :first_name, presence: :true, length: {maximum: 50}
  validates :last_name, presence: :true, length: {maximum: 50}

  has_many :tours
  has_many :reservations
  has_many :reviews

  def full_name
    "#{first_name} #{last_name}"
  end

  def self.from_omniauth(auth)
    u = User.where(email: auth.info.email).first
    return u if u

    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      ap auth.info
      user.provider = auth.provider
      user.uid = auth.uid
      user.first_name = auth.info.first_name || 'Caca'
      user.last_name = auth.info.last_name || 'Boudin'
      user.email = auth.info.email
      user.image = auth.info.image
      user.password = Devise.friendly_token[0,20]
    end
  end
end
