class Conversation < ApplicationRecord
  belongs_to :sender, class_name: 'User'
  belongs_to :recipient, class_name: 'User'

  has_many :messages, dependent: :destroy

  validates_uniqueness_of :sender_id, scope: :recipient_id

  scope :involving, ->(user) do
    where(sender: user).or(where(recipient: user))
  end

  scope :between, ->(sender, recipient) do
    where(sender: sender, recipient: recipient).or(where(sender: recipient, recipient: sender))
  end
end
