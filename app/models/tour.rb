class Tour < ApplicationRecord
  belongs_to :user
  has_many :photos
  has_many :reservations
  has_many :reviews

  geocoded_by :address
  after_validation :geocode, if: :address_changed?

  validates :category, presence: true
  validates :people_nb, presence: true
  validates :title, presence: true, length: {maximum: 50}
  validates :description, presence: true
  validates :address, presence: true

  def average_rating
    reviews.count == 0 ? 0 : reviews.average(:rating).round(2)
  end
end
