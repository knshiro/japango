class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :config_permitted_parameters, if: :devise_controller?

  protected

  def config_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :phone_number, :description, :email, :password])
  end
end
