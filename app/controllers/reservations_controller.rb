class ReservationsController < ApplicationController

  before_action :authenticate_user!

  def preload
    tour = Tour.find(params[:tour_id])
    today = Date.current
    reservations = tour.reservations.where('start_date >= ? OR end_date >= ?', today, today)

    render json: reservations
  end

  def preview
    return render(text: '', status: 400) unless params[:start_date].present? && params[:end_date].present?
    start_date = Date.parse(params[:start_date])
    end_date = Date.parse(params[:end_date])

    output = {
      conflict: is_conflict(start_date, end_date)
    }

    render json: output
  end

  def create
    @reservation = current_user.reservations.create(reservation_params)

    redirect_to @reservation.tour, notice: 'Your reservation has been created...'
  end

  def your_trips
    @reservations = current_user.reservations
  end

  def your_reservations
    @tours = current_user.tours.eager_load(:reservations)
  end

  private

  def is_conflict(start_date, end_date)
    tour = Tour.find(params[:tour_id])
    check = tour.reservations.where("? < start_date AND end_date < ?", start_date, end_date)
    check.exists?
  end

  def reservation_params
    params.require(:reservation).permit(:start_date, :end_date, :price, :total, :tour_id)
  end
end
