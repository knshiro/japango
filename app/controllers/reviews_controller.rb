class ReviewsController < ApplicationController
  
  def create
    @review = current_user.reviews.create(review_params)
    redirect_to @review.tour
  end

  def destroy
    @review = current_user.reviews.find(params[:id])
    tour = @review.tour
    @review.destroy

    redirect_to tour
  end

  private

  def review_params
    params.require(:review).permit(:comment, :rating, :tour_id)
  end

end
