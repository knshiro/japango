class PagesController < ApplicationController
  def home
    @tours = Tour.limit(3)
  end

  def search
    session[:loc_search] = params[:search].try(:strip).present? ? params[:search] : ''

    if session[:loc_search].present?
      tours = Tour.where(active: true).near(session[:loc_search], 5, order: 'distance')
    else
      tours = Tour.where(active: true).all
    end

    @search = tours.ransack(params[:q])
    @tours = @search.result

    @arrTours = @tours.to_a

    if (params[:start_date].present? && params[:end_date].present?)
      start_date = Date.parse(params[:start_date])
      end_date = Date.parse(params[:end_date])

      @tours.each do |tour|
        not_available = tour.reservations.where('
        (? <= start_date AND start_date <= ?)
        OR (? <= end_date AND end_date <= ?)
        OR ( start_date < ? AND ? < end_date)',
          start_date, end_date,
          start_date, end_date,
          start_date, end_date).exists?

        @arrTours.delete(tour) if not_available
      end
    end
  end
end
