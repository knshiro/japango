class MessagesController < ApplicationController

  before_action :authenticate_user!
  before_action :set_conversation
  before_action :set_messages

  def index
    @other = current_user == @conversation.sender ? @conversation.recipient : @conversation.sender
  end

  def create
    @message = @conversation.messages.new(message_params)

    if @message.save
      respond_to do |format|
        format.js
      end
    end
  end

  private

  def set_conversation
    @conversation = Conversation.involving(current_user).find(params[:conversation_id])
  end

  def set_messages
    @messages = @conversation.messages.order(created_at: :desc)
  end

  def message_params
    params.require(:message).permit(:content, :user_id)
  end

end
