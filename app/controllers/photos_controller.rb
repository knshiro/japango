class PhotosController < ApplicationController

  def destroy
    @photo = Photo.find(params[:id])
    tour = @photo.tour

    @photo.destroy
    @photos = Photo.where(tour_id: tour.id)

    respond_to :js
  end
end
