class ToursController < ApplicationController

  before_action :set_tour, only: [:show, :edit, :update]
  before_action :authenticate_user!, except: [:show]

  def index
    @tours = current_user.tours
  end

  def show
    @photos = @tour.photos
    @reviews = @tour.reviews

    if current_user
      @booked = current_user.reservations.where(tour: @tour).exists?
      @has_review = @reviews.find_by(user: current_user)
    end
  end

  def new
    @tour = current_user.tours.build
  end

  def create
    @tour = current_user.tours.build(tour_params)
    if @tour.save
      params[:images].each do |image|
        @tour.photos.create(image: image)
      end if params[:images]

      redirect_to edit_tour_path(@tour), notice: 'Saved!'
    else
      render :new
    end
  end

  def edit
    redirect_to root_path, notice: "You don't have permission" unless current_user.id == @tour.user.id
    @photos = @tour.photos
  end

  def update
    if @tour.update(tour_params)
      params[:images].each do |image|
        @tour.photos.create(image: image)
      end if params[:images]

      redirect_to edit_tour_path(@tour), notice: 'Updated!'
    else
      render :edit
    end
  end

  private

  def set_tour
    @tour = Tour.find(params[:id])
  end

  def tour_params
    params.require(:tour).permit(:category, :people_nb, :title, :description, :address, :price, :active)
  end
end
