module ToursHelper

  def first_photo_url(tour, format=nil)
    return '' unless tour.photos.any?

    image = tour.photos.first.image
    format.present? ? image.url(format) : image.url
  end

end
